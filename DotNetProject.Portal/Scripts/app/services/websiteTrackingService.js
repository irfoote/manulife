﻿(function () {
    'use strict';

    angular.module('websiteTracking.services', [])
    .service('WebsiteTrackingService', function ($http) {

        var WebsiteTrackingService = {};

        var apiUri = 'http://ec2-35-164-23-20.us-west-2.compute.amazonaws.com:8080';
        //var apiUri = 'http://localhost:50669';

        WebsiteTrackingService.top5 = function () {
            var retval = $http.get(apiUri + '/api/trackedwebsites/gettop5visitedwebsites').then(function (response) {
                return response.data;
            });
            return retval;
        };

        WebsiteTrackingService.top5ByDate = function (date) {
            
            var endpoint = apiUri + '/api/trackedwebsites/gettop5visitedwebsitesbydate?searchDate=' + date;
            console.log("endpoint :" + endpoint);
            var retval = $http.get(endpoint).then(function (response) {
                return response.data;
            });
            return retval;
        };

        WebsiteTrackingService.top5ByDateAndUri = function (date, uri) {
            
            var endpoint = apiUri + '/api/trackedwebsites/gettop5visitedwebsitesbydateanduri?searchDate=' + date + '&uri=' + uri;
            console.log("endpoint :" + endpoint);
            var retval = $http.get(endpoint).then(function (response) {
                return response.data;
            });
            return retval;
        };

        return WebsiteTrackingService;
    });
})();