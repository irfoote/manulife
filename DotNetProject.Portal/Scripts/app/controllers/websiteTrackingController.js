﻿(function () {
    'use strict';

    angular.module('websiteTracking.controllers', [
          'websiteTracking.services'
    ])
    .controller('WebsiteTrackingController', function ($scope, WebsiteTrackingService) {
        var currentDate = new Date();
        
        var init = function () {
            $scope.searchDate = currentDate.getFullYear() + '-' + (currentDate.getMonth() < 10 ? '0' + currentDate.getMonth() : currentDate.getMonth()) + '-' + currentDate.getDate();
            $scope.searchDateAsDate = new Date('2016-01-06T12:00:00');
            $scope.searchUri = "";
            
            $scope.searchWebsites();
        };

        $scope.searchWebsites = function () {
            var searchParam = $scope.searchDateAsDate.getFullYear() + '-' + ($scope.searchDateAsDate.getMonth() + 1 < 10 ? '0' + ($scope.searchDateAsDate.getMonth() + 1) : ($scope.searchDateAsDate.getMonth() + 1)) + '-' + $scope.searchDateAsDate.getDate();
            var promise = WebsiteTrackingService.top5ByDateAndUri(searchParam, $scope.searchUri);
            promise.then(function (data) {
                $scope.searchResults = data;
            });
        };

        init();

    });
    
})();