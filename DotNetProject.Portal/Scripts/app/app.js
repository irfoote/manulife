﻿(function () {
    'use strict';

    angular.module('websiteTracking', [
      'ui.router',
      'websiteTracking.services',
      'websiteTracking.controllers'
    ])
    .config(function ($urlRouterProvider) {
        $urlRouterProvider.otherwise("/");
    });


})();
/*
var app = angular.module('websiteTrackingApp', []);

app.controller('websiteTrackingController', function ($scope, $http) {
    var currentDate = new Date();
    $scope.searchDate = currentDate.getFullYear() + '-' + (currentDate.getMonth() < 10 ? '0' + currentDate.getMonth() : currentDate.getMonth()) + '-' + currentDate.getDate();

    $http.get('http://localhost:50669/api/trackedwebsites/gettop5visitedwebsites').then(function (response) {
        $scope.searchResults = response.data;
    });
});
*/