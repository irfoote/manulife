namespace DotNetProject.Core.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DotNetProject.Core.TrackingContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DotNetProject.Core.TrackingContext context)
        {
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-06"), URI = "www.bing.com", VisitCount = 14065457 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-06"), URI = "www.ebay.com.au", VisitCount = 19831166 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-06"), URI = "www.facebook.com", VisitCount = 104346720 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-06"), URI = "mail.live.com", VisitCount = 21536612 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-06"), URI = "www.wikipedia.org", VisitCount = 13246531 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-27"), URI = "www.ebay.com.au", VisitCount = 23154653 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-06"), URI = "au.yahoo.com", VisitCount = 11492756 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-06"), URI = "www.google.com", VisitCount = 26165099 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-13"), URI = "www.youtube.com", VisitCount = 68487810 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-27"), URI = "www.wikipedia.org", VisitCount = 16550230 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-06"), URI = "ninemsn.com.au", VisitCount = 21734381 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-20"), URI = "mail.live.com", VisitCount = 24344783 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-20"), URI = "www.ebay.com.au", VisitCount = 22598506 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-27"), URI = "mail.live.com", VisitCount = 24272437 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-27"), URI = "www.bing.com", VisitCount = 16041776 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-20"), URI = "ninemsn.com.au", VisitCount = 24241574 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-20"), URI = "www.facebook.com", VisitCount = 118984483 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-27"), URI = "ninemsn.com.au", VisitCount = 24521168 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-27"), URI = "www.facebook.com", VisitCount = 123831275 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-20"), URI = "www.bing.com", VisitCount = 16595739 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-13"), URI = "www.facebook.com", VisitCount = 118506019 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-20"), URI = "www.google.com.au", VisitCount = 170020924 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-27"), URI = "www.youtube.com", VisitCount = 69327140 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-13"), URI = "mail.live.com", VisitCount = 24772355 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-13"), URI = "ninemsn.com.au", VisitCount = 24555033 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-20"), URI = "www.google.com", VisitCount = 28996455 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-13"), URI = "www.bing.com", VisitCount = 16618315 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-27"), URI = "www.google.com.au", VisitCount = 171842376 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-06"), URI = "www.youtube.com", VisitCount = 59811438 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-13"), URI = "www.netbank.commbank.com.au", VisitCount = 13316233 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-20"), URI = "www.netbank.commbank.com.au", VisitCount = 13072234 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-13"), URI = "www.ebay.com.au", VisitCount = 22785028 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-20"), URI = "www.wikipedia.org", VisitCount = 16519992 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-27"), URI = "www.bom.gov.au", VisitCount = 14369775 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-27"), URI = "www.google.com", VisitCount = 29422150 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-20"), URI = "www.youtube.com", VisitCount = 69064107 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-06"), URI = "www.google.com.au", VisitCount = 151749278 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-13"), URI = "www.wikipedia.org", VisitCount = 16015926 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-13"), URI = "www.google.com", VisitCount = 29203671 });
            context.TrackedWebsites.Add( new TrackedWebsite() { LogDate = DateTime.Parse("2016-01-13"), URI = "www.google.com.au", VisitCount = 172220397 });
            context.SaveChanges();
        }
    }
}
