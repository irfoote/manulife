namespace DotNetProject.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TrackedWebsites",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        URI = c.String(),
                        LogDate = c.DateTime(nullable: false),
                        VisitCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TrackedWebsites");
        }
    }
}
