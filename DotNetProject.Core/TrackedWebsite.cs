﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetProject.Core
{
    public class TrackedWebsite
    {
        public int ID { get; set; }
        public string URI { get; set; }
        public DateTime LogDate { get; set; }
        public int VisitCount { get; set; }

    }
}
