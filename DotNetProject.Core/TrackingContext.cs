﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DotNetProject.Core
{
    public class TrackingContext : DbContext
    {
        public TrackingContext() : base("TrackingContext")
        { }

        public DbSet<TrackedWebsite> TrackedWebsites { get; set; }

    }
}
