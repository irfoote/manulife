﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DotNetProject.Core;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Description;
using System.Web.Http.Cors;

namespace DotNetProject.API.Controllers
{
    [EnableCors(origins: "http://ec2-35-164-23-20.us-west-2.compute.amazonaws.com", headers: "*", methods: "*")]
    public class TrackedWebsitesController : ApiController
    {
        private TrackingContext db = new Core.TrackingContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }

            base.Dispose(disposing);
        }

        [AcceptVerbs("GET")]
        public IEnumerable<TrackedWebsite> GetTop5VisitedWebsitesByDate(string searchDate)
        {
            DateTime parsedDate = Convert.ToDateTime(searchDate);

            return db.TrackedWebsites
                .Where(x => x.LogDate == parsedDate)
                .OrderByDescending(x => x.VisitCount)
                .Take(5);
        }

        [AcceptVerbs("GET")]
        public IEnumerable<TrackedWebsite> GetTop5VisitedWebsites()
        {
            return db.TrackedWebsites
                .OrderByDescending(x => x.VisitCount)
                .Take(5);
        }



        [AcceptVerbs("GET")]
        public IEnumerable<TrackedWebsite> GetTop5VisitedWebsitesByDateAndURI(string searchDate, string uri)
        {
            DateTime parsedDate = Convert.ToDateTime(searchDate);
            bool searchUri = false;
            if(uri != null)
            {
                if(uri.Length > 0)
                {
                    searchUri = true;
                }
            }

            if (searchUri)
            {
                return db.TrackedWebsites
                    .Where(x => x.LogDate == parsedDate)
                    .Where(x => x.URI.Contains(uri))
                    .OrderByDescending(x => x.VisitCount)
                    .Take(5);
                
            }
            
            else
            {
                return db.TrackedWebsites
                .Where(x => x.LogDate == parsedDate)
                .OrderByDescending(x => x.VisitCount)
                .Take(5);
            }

            
        }
    }
}
 